//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// battery.cpp
//
// implementation of CHudHunger class
//
#include "cbase.h"
#include "c_sdk_player.h"
#include "hud.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "hud_numericdisplay.h"
#include "iclientmode.h"

#include "vgui_controls/AnimationController.h"
#include "vgui/ILocalize.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define INIT_BAT	-1

//-----------------------------------------------------------------------------
// Purpose: Displays suit power (armor) on hud
//-----------------------------------------------------------------------------
class CHudHunger : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudHunger, CHudNumericDisplay );

public:
	CHudHunger( const char *pElementName );
	void Init( void );
	void Reset( void );
	void VidInit( void );
	void OnThink( void );
};

DECLARE_HUDELEMENT( CHudHunger );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudHunger::CHudHunger( const char *pElementName ) : BaseClass(NULL, "HudHunger"), CHudElement( pElementName )
{
	SetHiddenBits( HIDEHUD_HEALTH | HIDEHUD_NEEDSUIT );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHunger::Init( void )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHunger::Reset( void )
{
	wchar_t *tempString = g_pVGuiLocalize->Find("#Hud_Hunger");

	if (tempString)
	{
		SetLabelText(tempString);
	}
	else
	{
		SetLabelText(L"HUNGER");
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHunger::VidInit( void )
{
	Reset();
}

void CHudHunger::OnThink( void )
{
	CSDKPlayer *pPlayer = CSDKPlayer::GetLocalSDKPlayer();
	if ( pPlayer )
		SetDisplayValue(pPlayer->GetHunger());
}