//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// battery.cpp
//
// implementation of CHudThirst class
//
#include "cbase.h"
#include "c_sdk_player.h"
#include "hud.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "hud_numericdisplay.h"
#include "iclientmode.h"

#include "vgui_controls/AnimationController.h"
#include "vgui/ILocalize.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define INIT_BAT	-1

//-----------------------------------------------------------------------------
// Purpose: Displays suit power (armor) on hud
//-----------------------------------------------------------------------------
class CHudThirst : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudThirst, CHudNumericDisplay );

public:
	CHudThirst( const char *pElementName );
	void Init( void );
	void Reset( void );
	void VidInit( void );
	void OnThink( void );
};

DECLARE_HUDELEMENT( CHudThirst );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudThirst::CHudThirst( const char *pElementName ) : BaseClass(NULL, "HudThirst"), CHudElement( pElementName )
{
	SetHiddenBits( HIDEHUD_HEALTH | HIDEHUD_NEEDSUIT );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudThirst::Init( void )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudThirst::Reset( void )
{
	wchar_t *tempString = g_pVGuiLocalize->Find("#Hud_Thirst");

	if (tempString)
	{
		SetLabelText(tempString);
	}
	else
	{
		SetLabelText(L"Thirst");
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudThirst::VidInit( void )
{
	Reset();
}

void CHudThirst::OnThink( void )
{
	CSDKPlayer *pPlayer = CSDKPlayer::GetLocalSDKPlayer();
	if ( pPlayer )
		SetDisplayValue(pPlayer->GetThirst());
}