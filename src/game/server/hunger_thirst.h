class HungerThirst
{
public:
	int m_iHunger;
	int m_iThirst;

	HungerThirst(int hunger, int thirst);

	int IncHunger(int hunger);
	int IncThirst(int thirst);

	struct CounterParams_t;
	unsigned Counter(void *params);
	void StartCounter(int hunger_inc, int thirst_inc);

	void GetHunger();
	void GetThirst();
};