#include "cbase.h"
#include "hunger_thirst.h"
#include "threads.h"
#include "threadtools.h"

HungerThirst::HungerThirst(int hunger, int thirst) {
	m_iHunger = hunger;
	m_iThirst = thirst;
}

int HungerThirst::IncHunger(int hunger) {
	m_iThirst = hunger + 1;
	return m_iHunger;
}

int HungerThirst::IncThirst(int thirst) {
	m_iThirst = thirst + 1;
	return m_iThirst;
}

struct CounterParams_t {
	int h_inc;
	int t_inc;
	int hunger;
	int thirst;
};

unsigned Counter(void *params) {
	CounterParams_t* vars = (CounterParams_t*) params;

	int h_inc = vars->h_inc;
	int t_inc = vars->t_inc;
	int hunger = 1;
	int thirst = 1;

	while (true) {
		ThreadSleep(h_inc);
		hunger++;
		vars->hunger = hunger;
		ThreadSleep(t_inc);
		thirst++;
		vars->thirst = thirst;
	}
}

void StartCounter(int hunger_inc, int thirst_inc) {
	CounterParams_t* vars = new CounterParams_t;
	vars->h_inc = 10000;
	vars->t_inc = 5000;
	CreateSimpleThread( Counter, vars );
}