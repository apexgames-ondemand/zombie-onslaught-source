#include "cbase.h"
#include "ge_pytest.h"

CMYPyHandle *g_PyHandle = NULL;
CMYPyHandle *GetPyHandle()
{
	return g_PyHandle;
}


CMYPyHandle::CMYPyHandle()
{
	g_MyHandle = this;
	//make sure we call this!
	Register();
}

void CMYPyHandle::Init()
{
	try
	{
		ExecFile("test.py");
	}
	catch (...)
	{
		HandlePythonException();
	}
}

void CMYPyHandle::Shutdown()
{
}

pyExampleI* CMYPyHandle::GetMyClass()
{
	return dynamic_cast<pyExampleI*>();
}

#include "ge_pytest.h"

CONCOMMAND(python_test, "Test out python!")
{
	pyExampleI* pyClass = GetPyHandle()->GetMyClass();
	
	pyClass->sayHallo();
	
	Msg(pyClass->getMsg());
	Msg("\n");
	
	pyClass->setMsg("This is a test Message!");
	
	Msg(pyClass->getMsg());
	Msg("\n");
}
